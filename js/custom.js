class LoadFileContent {
    constructor(tag, attr, dir) {
        this.tag = tag;
        this.attr = attr;
        this.dir = dir;
        this.http;
		        
        if (window.XMLHttpRequest) { 
            this.http = new XMLHttpRequest(); 
        } else { 
            this.http = new ActiveXObject("Microsoft.XMLHTTP"); 
        }
    }
    
    request(file) {
        this.http.open('GET', `./${this.dir}/${file}`, false);
        this.http.send();
        
        if (this.http.status != 200) {
            return this.http.status;
        } else {
            return this.http.responseText;
        }
    }
    
    load() {
        var blocks = document.getElementsByTagName(this.tag);
        for (var i = 0; i < blocks.length; i++) {
            var attribute = blocks[i].dataset[this.attr];
            if (attribute) {
                blocks[i].innerHTML = this.request(attribute);
            }
        }
    }
}